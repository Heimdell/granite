{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}
module AST.Types where

import Data.Text (Text)

import Name
import Data.Generics.Uniplate.Operations (transformBiM)
import Data.Generics.Uniplate.Data ()
import Control.Monad.State (StateT(runStateT), runState, State)
import Data.Data (Data)
import GHC.Generics (Generic)
import Data.Function ((&))

data Module = Module
  { mName    :: [Name]
  , mImports :: [Import]
  , mDecls   :: [Private TopLevelDecl]
  }
  deriving stock (Data, Generic)

data Import = Import
  { iPath   :: [Name]
  , iRename :: Maybe [Name]
  , iItems  :: [Name]
  }
  deriving stock (Data, Generic)

-- | Types of types.
--
data Kind
  = KStar             -- ^ simple type
  | KList  Kind       -- ^ a list of types
  | KArrow Kind Kind  -- ^ a type constructor
  | KVar   Name       -- ^ yet unknown kind
  | KHole             -- ^ don't care
  | KConstraint
  deriving stock (Data, Generic)

-- | Types of programs.
--
data Type
  = TVar       Name       -- ^ type variable
  | TConst     QName      -- ^ known type
  | TRigid     Name      -- ^ known type
  | TArrow     Type Type       -- ^ type of functions
  | TApp       Type Type       -- ^ application of type constructor
  | TForall    Name Kind [Type] Type  -- ^ higher-rank type
  | TListEmpty                 -- ^ empty list of types
  | TListCons  Type Type       -- ^ attach type to a list of types
  | THole                      -- ^ don't care
  deriving stock (Data, Generic)

data QName = QName
  { qPath :: [Name]
  , qName ::  Name
  }
  deriving stock (Eq, Ord)
  deriving stock (Data, Generic)

-- | Programs.
--
data Prog
  = Var     QName                 -- ^ variable
  | Sym     QName [(Name, Maybe Prog)]  -- ^ literal for userspace types
  | App     Prog   Prog          -- ^ function call
  | APP     Prog   Type          -- ^ type application
  | Lam   [(Name,  Type)] Prog   -- ^ lambda-function
  | LAM   [(Name,  Kind)] [Type] Prog   -- ^ /\-function (type-parametrised program)
  | Let    [Decl]  Prog          -- ^ declaration block
  | Match   Prog  [Alt]          -- ^ case analysis
  | Lit     Lit                  -- ^ literal
  | HasType Prog   Type
  | List  [(Bool,  Prog)]
  deriving stock (Data, Generic)

-- | Declarations.
--
data TopLevelDecl
  = Class [Type] Name [(Name, Kind)] [DeclSig]
  | Instance [Type] Name [Type] [Decl]
  | Decl Decl
  | Type Name [(Name, Kind)] Kind [Ctor]  -- ^ type declaration
  | TypeAlias Name [(Name, Kind)] Kind Type  -- ^ type declaration
  deriving stock (Data, Generic)

data Decl
  = Val Name Type Prog    -- ^ value declaration
  deriving stock (Data, Generic)

data Private a = Private Bool a
  deriving stock (Data, Generic)

data DeclSig
  = ValSig  Name Type  -- ^ value declaration
  | TypeSig Name Kind  -- ^ type declaration
  deriving stock (Data, Generic)

-- | Value constructor.
--
data Ctor
  = Ctor
    { cName   ::   Name          -- ^ consructor name
    , cFields :: [(Name, Type)]  -- ^ constructor fields
    , cType   ::   Type          -- ^ GADT-type
    }
  deriving stock (Data, Generic)

data Alt
  = Alt Pat Prog
  deriving stock (Data, Generic)

data Pat
  = PVar Name
  | PSym QName [(Name, Maybe Pat)]
  | PLit Lit
  | PList [Pat] (Maybe Pat)
  | PWild
  deriving stock (Data, Generic)

data Lit
  = I Integer
  | F Double
  | S Text
  deriving stock (Data, Generic)

tvar :: Name -> Type
tvar = TVar

tcon :: Name -> Type
tcon n = TConst (QName [] n)

(-->) :: Type -> Type -> Type
(-->) = TArrow

($:) :: Type -> Type -> Type
($:) = TApp

nil :: Type
nil = TListEmpty

(!:) :: Type -> Type -> Type
(!:) = TListCons

infixr 1 -->
infixl 2 $:
infixr 3 !:

removeHoles :: Module -> (Module, Fresh)
removeHoles module_ =
  runState
    do module_
        & transformBiM removeKHole
        >>= transformBiM removeTHole
    do Fresh 0
  where
    removeKHole :: Kind -> State Fresh Kind
    removeKHole = \case
      KHole -> KVar <$> refresh "k"
      other -> return other

    removeTHole :: Type -> State Fresh Type
    removeTHole = \case
      THole -> TVar <$> refresh "t"
      other -> return other
