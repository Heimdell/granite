
module AST.Subst where

import AST.Types
import AST.Pretty ()
import Pretty
import Name
import Data.Map (Map)
import Data.Map qualified as Map
import Data.Set (Set)
import Data.Set qualified as Set
import Data.Maybe (fromMaybe)

freeKindVars :: Kind -> Set Name
freeKindVars = \case
  KVar   name -> Set.singleton name
  KList  k    -> freeKindVars k
  KArrow a b  -> freeKindVars a <> freeKindVars b
  _           -> mempty

freeTypeVars :: Type -> Set Name
freeTypeVars = \case
  TVar na -> Set.singleton na
  TConst _qn -> mempty
  TRigid _qn -> mempty
  TArrow ty ty' -> freeTypeVars ty <> freeTypeVars ty'
  TApp ty ty' -> freeTypeVars ty <> freeTypeVars ty'
  TForall na _ki tys ty -> (foldMap freeTypeVars tys <> freeTypeVars ty) `Set.difference` Set.singleton na
  TListEmpty -> mempty
  TListCons ty ty' -> freeTypeVars ty <> freeTypeVars ty'
  THole -> error "freeTypeVars: type hole remains in code"

data Subst = Subst
  { cKinds   :: Map  Name Kind
  , cTypes   :: Map  Name Type
  , cRigids  :: Map  Name Type
  }
  deriving (Show) via PP Con Subst

instance Pretty Con Subst where
  prettyPrec _ Subst {cKinds, cTypes, cRigids} =
    color Yellow "substitutions" <:> vsep
      do concat
          [ block' (color Green   "types")  cTypes
          , block' (color Red     "kinds")  cKinds
          , block' (color Magenta "rigids") cRigids
          ]
    where
      block' hdr m =
        [hdr <:> vsep (map is (Map.toList m)) | not (Map.null m)]

      is (n, k) = pretty n <+> color Green ":=" <:> pretty k

substTopLevel :: Subst -> TopLevelDecl -> TopLevelDecl
substTopLevel s = \case
  Class     tys na x0   dss -> Class    (map (substTypes s) tys) na ((fmap.fmap) (substKinds s) x0) (fmap (substSig s) dss)
  Instance  tys na tys' des -> Instance (map (substTypes s) tys) na (fmap (substTypes s) tys') (fmap (substDecl s) des)
  Decl      de              -> Decl     (substDecl s de)
  Type      na x0 ki cts    -> Type      na ((fmap.fmap) (substKinds s) x0) (substKinds s ki) (fmap (substCtor s) cts)
  TypeAlias na x0 ki ty     -> TypeAlias na ((fmap.fmap) (substKinds s) x0) (substKinds s ki) (substTypes s ty)

substCtor :: Subst -> Ctor -> Ctor
substCtor s = \case
  Ctor na x0 ty -> Ctor na ((fmap.fmap) (substTypes s) x0) (substTypes s ty)

substSig :: Subst -> DeclSig -> DeclSig
substSig s = \case
  ValSig  na ty -> ValSig  na (substTypes s ty)
  TypeSig na ki -> TypeSig na (substKinds s ki)

substKinds :: Subst -> Kind -> Kind
substKinds Subst {cKinds} = go
  where
    go :: Kind -> Kind
    go = \case
      KStar         -> KStar
      KList ki      -> KList (go ki)
      KArrow ki ki' -> KArrow (go ki) (go ki')
      KVar na       -> search KVar na cKinds
      KHole         -> error "substKinds: kind hole remains in code"
      KConstraint   -> KConstraint

substTypes :: Subst -> Type -> Type
substTypes s@Subst {cTypes, cRigids} = go
  where
    go :: Type -> Type
    go = \case
      TVar na -> search TVar na cTypes
      TConst qn -> TConst qn
      TRigid qn -> search TRigid qn cRigids
      TArrow ty ty' -> TArrow (go ty) (go ty')
      TApp ty ty' -> TApp (go ty) (go ty')
      TForall na ki tys ty -> TForall na ki (map (substTypes s') tys) (substTypes s' ty)
        where
          s' = s { cTypes = Map.delete na cTypes }
      TListEmpty -> TListEmpty
      TListCons ty ty' -> TListCons (go ty) (go ty')
      THole -> error "substType: type hole remains in code"

subst :: Subst -> Prog -> Prog
subst s = go
  where
    go :: Prog -> Prog
    go = \case
      Var qn        -> Var qn
      Sym qn x0     -> Sym qn ((fmap.fmap.fmap) go x0)
      App pr pr'    -> App (go pr) (go pr')
      APP pr ty     -> APP (go pr) (substTypes s ty)
      Lam x0 pr     -> Lam ((fmap.fmap) (substTypes s) x0) (go pr)
      LAM x0 tys pr -> LAM ((fmap.fmap) (substKinds s) x0) (fmap (substTypes s') tys) (subst s' pr)
        where
          s' = s { cTypes = foldr (Map.delete . fst) (cTypes s) x0 }
      Let des pr    -> Let (fmap (substDecl s) des) (go pr)
      Match pr alts -> Match (go pr) (fmap (substAlt s) alts)
      Lit lt        -> Lit lt
      HasType pr ty -> HasType (go pr) (substTypes s ty)
      List x0       -> List ((fmap.fmap) go x0)

substAlt :: Subst -> Alt -> Alt
substAlt s = \case
  Alt pat pr -> Alt pat (subst s pr)

substDecl :: Subst -> Decl -> Decl
substDecl s = \case
  Val na ty pr -> Val na (substTypes s ty) (subst s pr)

search :: Ord k => (k -> v) -> k -> Map k v -> v
search def k m = fromMaybe (def k) (Map.lookup k m)

instance Semigroup Subst where
  s1 <> s2 = Subst
    { cTypes   = Map.map (substTypes s1) (cTypes s2) <> cTypes s1
    , cKinds   = Map.map (substKinds s1) (cKinds s2) <> cKinds s1
    , cRigids  = cRigids s1 <> cRigids s2
    }

instance Monoid Subst where
  mempty = Subst
    { cTypes  = mempty
    , cKinds  = mempty
    , cRigids = mempty
    }
