
module AST.Unify where

import Control.Monad.State

import Data.Set qualified as Set
import Data.Map qualified as Map
import Data.Function ((&))

import AST.Types
import AST.Subst
import AST.Pretty ()
import Name
import Control.Exception (Exception)
import Control.Monad.Catch (MonadThrow (throwM))

data UnificationError struct
  = Mismatch struct struct
  | Cyclic   Name   struct
  deriving stock (Show)

data UError
  = KindError (UnificationError Kind)
  | TypeError (UnificationError Type)
  deriving stock (Show)
  deriving anyclass (Exception)

type UnifyM = StateT Fresh (Either UError)
type MonadUnify m =
    ( MonadState Fresh m
    , MonadThrow m
    )

runUnifyM :: Int -> UnifyM a -> IO a
runUnifyM fresh act = evalStateT act (Fresh fresh) & either (error . show) return

unifiedKind :: MonadUnify m => (Kind, Kind) -> m (Kind, Subst)
unifiedKind (a, b) = do
  s <- unifyKinds (a, b)
  return (substKinds s a, s)

unifyKinds :: MonadUnify m => (Kind, Kind) -> m Subst
unifyKinds = \case
  (KHole, _) -> error "unifyKinds: KHole remains in code"
  (_, KHole) -> error "unifyKinds: KHole remains in code"

  (KConstraint, KConstraint) -> return mempty

  (KStar, KStar) -> do
    return mempty

  (KList t, KList u) -> do
    unifyKinds (t, u)

  (KArrow a b, KArrow c d) -> do
    s1 <- unifyKinds (a, c)
    s2 <- unifyKinds (substKinds s1 b, substKinds s1 d)
    return (s1 <> s2)

  (KVar a, KVar b) -> do
    return mempty
      { cKinds = Map.singleton a (KVar b)
      }

  (KVar a, b)
    | a `Set.member` freeKindVars b -> do
      throwM $ KindError $ Cyclic a b
    | otherwise -> do
      return mempty
        { cKinds = Map.singleton a b }

  (b, KVar a)
    | a `Set.member` freeKindVars b -> do
      throwM $ KindError $ Cyclic a b
    | otherwise -> do
      return mempty
        { cKinds = Map.singleton a b }

  (a, b) -> throwM $ KindError $ Mismatch a b

unifyTypes :: MonadUnify m => (Type, Type) -> m Subst
unifyTypes = \case
  (THole, _) -> error "unifyTypes: THole remains in code"
  (_, THole) -> error "unifyTypes: THole remains in code"

  (TVar a, TVar b) -> do
    return mempty { cTypes = Map.singleton a (TVar b) }

  (TVar a, b)
    | a `Set.member` freeTypeVars b -> do
      throwM $ TypeError $ Cyclic a b
    | otherwise -> do
      return mempty
        { cTypes = Map.singleton a b }

  (b, TVar a)
    | a `Set.member` freeTypeVars b -> do
      throwM $ TypeError $ Cyclic a b
    | otherwise -> do
      return mempty
        { cTypes = Map.singleton a b }

  (TConst a, TConst b)
    | a == b -> do
      return mempty

  (TRigid a, TRigid b)
    | a == b -> do
      return mempty

  (TArrow a b, TArrow c d) -> do
    s1 <- unifyTypes (a, c)
    s2 <- unifyTypes (substTypes s1 b, substTypes s1 d)
    return (s1 <> s2)

  (TApp a b, TApp c d) -> do
    s1 <- unifyTypes (a, c)
    s2 <- unifyTypes (substTypes s1 b, substTypes s1 d)
    return (s1 <> s2)

  (TForall n k _ a, TForall m k' _ b) -> do
    s <- unifyKinds (k, k')
    o <- refresh n
    let s1 = s <> mempty { cRigids = Map.fromList [(n, TRigid o), (m, TRigid o)] }
    s2 <- unifyTypes (substTypes s a, substTypes s b)
    return (s <> s1 <> s2)

  (TForall n _ _ ty, b) -> do
    a <- instantiate n ty
    unifyTypes (a, b)

  (TListEmpty, TListEmpty) -> do
    return mempty

  (TListCons a b, TListCons c d) -> do
    s1 <- unifyTypes (a, c)
    s2 <- unifyTypes (substTypes s1 b, substTypes s1 d)
    return (s1 <> s2)

  (a, b) -> do
    throwM $ TypeError $ Mismatch a b

instantiate :: MonadState Fresh m => Name -> Type -> m Type
instantiate n ty = do
  m <- refresh n
  return $ substTypes mempty { cRigids = Map.singleton n (TVar m) } ty
