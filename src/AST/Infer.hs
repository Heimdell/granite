{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use newtype instead of data" #-}

module AST.Infer where

import Data.Map qualified as Map
import Data.Map (Map)

import AST.Types
import AST.Pretty ()
import AST.Subst
import AST.Unify
import Name
import Control.Monad.Reader (ReaderT (runReaderT), asks, MonadReader (local))
import Control.Monad.State (StateT, evalStateT, MonadState)
import Data.Function ((&))
import Control.Monad.Catch (throwM, Exception, MonadThrow)
import Data.Foldable (Foldable(fold))
import Control.Monad.Error (MonadError(throwError))
import Data.Maybe (isJust, fromJust, fromMaybe)
import Data.List (partition)
import Control.Arrow (first)

data Context = Context
  { ctxTypes :: Map QName Type
  , ctxKinds :: Map  Name Kind
  , ctxConst :: Map QName Kind
  , ctxCtors :: Map QName Ctor
  }

emptyCtx :: Context
emptyCtx = Context {ctxTypes = mempty, ctxKinds = mempty, ctxConst = mempty }

data ScopeError
  = Undefined QName
  | MissingFields Name [Name]
  deriving stock (Show)
  deriving anyclass (Exception)

type InferM = ReaderT Context (StateT Fresh IO)
type MonadInfer m =
  ( MonadReader Context m
  , MonadState  Fresh   m
  , MonadThrow          m
  )

runInferM :: Context -> Int -> InferM a -> IO a
runInferM ctx fresh act =
  act
    & flip runReaderT ctx
    & flip evalStateT (Fresh fresh)

inferKind :: MonadInfer m => Type -> m (Kind, Subst)
inferKind = \case
  TVar na -> do
    asks (Map.lookup na . ctxKinds) >>= \case
       Nothing -> throwM $ Undefined $ QName [] na
       Just ki -> return (ki, mempty)

  TConst qn -> do
    asks (Map.lookup qn . ctxConst) >>= \case
       Nothing -> throwM $ Undefined qn
       Just ki -> return (ki, mempty)

  TRigid na -> do
    asks (Map.lookup na . ctxKinds) >>= \case
       Nothing -> throwM $ Undefined $ QName [] na
       Just ki -> return (ki, mempty)

  TArrow ty ty' -> do
    (kf, s1) <- inferKind ty
    (kx, s2) <- inferKind (substTypes s1 ty')
    let s2' = s1 <> s2
    s3 <- unifyKinds (substKinds s2' kf, KStar)
    let s3' = s2' <> s3
    s4 <- unifyKinds (substKinds (s1 <> s2 <> s3) kx, KStar)
    let s4' = s3' <> s4
    return (KStar, s4')

  TApp ty ty' -> do
    (kf, s1) <- inferKind ty
    (kx, s2) <- inferKind (substTypes s1 ty')
    let s2' = s1 <> s2
    kr <- refresh "t"
    s3 <- unifyKinds (substKinds s2' kf, substKinds s2' $ kx `KArrow` KVar kr)
    let s3' = s2' <> s3
    return (substKinds s3' (KVar kr), s3')

  TForall na ki _ ty -> do
    local
      do \ctx@Context {ctxKinds} -> ctx { ctxKinds = Map.insert na ki ctxKinds }
      do inferKind ty

  TListEmpty -> do
    k <- refresh "k"
    return (KList (KVar k), mempty)

  TListCons ty ty' -> do
    (kx, s1) <- inferKind ty
    (kxs, s2) <- inferKind $ substTypes s1 ty'
    let s2' = s1 <> s2
    s3 <- unifyKinds (KList $ substKinds s2' kx, kxs)
    let s3' = s2' <> s3
    return (substKinds s3' kxs, s3')

  THole -> do
    error "inferKind: THole remains in code"

inferType :: MonadInfer m => Prog -> m (Type, Subst)
inferType = \case
  Var qn -> do
    asks (Map.lookup qn . ctxTypes) >>= \case
       Nothing -> throwM $ Undefined qn
       Just ty -> return (ty, mempty)
  Sym qn x0 -> do
    asks (Map.lookup qn . ctxCtors) >>= \case
       Nothing -> throwM $ Undefined qn
       Just Ctor {cName, cFields, cType} -> do
         s <- matchInitialisers cName x0 cFields
         return (substTypes s cType, s)
  App pr pr' -> _
  APP pr ty -> _
  Lam x0 pr -> _
  LAM x0 tys pr -> _
  Let des pr -> _
  Match pr alts -> _
  Lit lit -> _
  HasType pr ty -> _
  List x0 -> _

matchInitialisers :: MonadInfer m => Name -> [(Name, Maybe Prog)] -> [(Name, Type)] -> m Subst
matchInitialisers ctor fields fieldTys = do
  case zipByNamesFromRight fields fieldTys of
    Right fieldsAndTypes -> do
      foldForM fieldsAndTypes \(prog, ty) -> do
        (ty', s) <- inferType prog
        s1 <- unifyTypes (ty', ty)
        return (s <> s1)

    Left missing -> do
      throwM $ MissingFields ctor missing

zipByNamesFromRight :: [(Name, Maybe Prog)] -> [(Name, Type)] -> Either [Name] [(Prog, Type)]
zipByNamesFromRight actual required = do
  let
    uncaptured = actual
      & map do \(n, mp) -> (n, fromMaybe (Var $ QName [] n) mp)

    search' = required
      & map do \(n, t) -> (n, (lookup n uncaptured, t))

    (good, bad) = search'
      & partition do isJust . fst . snd

  if null bad
  then do
    return $ map (first fromJust . snd) good

  else do
    throwError $ map fst bad

foldForM :: (Traversable f, Applicative m, Monoid b) => f a -> (a -> m b) -> m b
foldForM box f = fold <$> traverse f box
