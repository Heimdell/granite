
module AST.Parser where

import Control.Monad (void, guard)
import Control.Monad.Combinators.Expr qualified as OP
import Data.Functor (($>), (<&>))
import Data.String (IsString(fromString))
import Data.Text (Text)
import Data.Void (Void)
import Text.Megaparsec
import Text.Megaparsec.Char qualified as LC
import Text.Megaparsec.Char.Lexer qualified as L

import AST.Types
import AST.Pretty ()
import Name (Name)
import qualified Data.Text.IO as Text

type Parser = Parsec Void Text

runParse :: Parser a -> String -> Text -> IO a
runParse p s t = either (error . errorBundlePretty) return $ parse p s t

runParseFile :: String -> IO Module
runParseFile fp = do
  txt <- Text.readFile fp
  runParse (pModule <* eof) fp txt

pModule :: Parser Module
pModule = do
  space'
  path <- option [] do
    symbol' "module"
    path <- sym `sepBy1` symbol' "."
    symbol' "where"
    return path
  imports <- many do pImport <* colon
  decls <- many do pPrivate pTopLevelDecl <* colon
  return $ Module path imports decls

pImport :: Parser Import
pImport = do
  symbol' "import"
  path <- sym `sepBy1` symbol' "."
  pathAs <- option Nothing do
    symbol' "as"
    Just <$> sym `sepBy1` symbol' "."
  items <- option [] do
    block do
      var <|> sym
  return $ Import path pathAs items

pTopLevelDecl :: Parser TopLevelDecl
pTopLevelDecl = choice
  [ pDeclLifted
  , pDeclTypeAlias
  , pDeclType
  , pClass
  , pInstance
  ]

pInstance :: Parser TopLevelDecl
pInstance = do
  symbol' "instance"
  reqs <- parens do
    pType `sepBy` colon

  symbol' "=>"
  name <- sym
  args <- many tTerm
  symbol' "where"
  decls <- block pDecl
  return $ Instance reqs name args decls

pPrivate :: Parser a -> Parser (Private a)
pPrivate p = do
  priv <- try do
    option False do
      symbol' "private"
      return True
  Private priv <$> p

pClass :: Parser TopLevelDecl
pClass = do
  symbol' "class"

  reqs <- option [] do
    parens do
        pType `sepBy` colon
      <* symbol' "=>"

  name <- sym
  args <- many pTypeArg
  symbol' "where"
  declSigs <- block pSig
  return $ Class reqs name args declSigs

pSig :: Parser DeclSig
pSig = choice
  [ pDeclSigVal
  , pDeclSigType
  ]

pDeclSigType :: Parser DeclSig
pDeclSigType = do
  symbol' "type"
  name <- sym
  symbol' ":"
  TypeSig name <$> pKind

pDeclSigVal :: Parser DeclSig
pDeclSigVal = do
  name <- var
  ty   <- option THole do
    symbol' ":"
    pType
  return (ValSig name ty)


pDeclTypeAlias :: Parser TopLevelDecl
pDeclTypeAlias = do
  try do
    symbol' "type"
    symbol' "alias"
  name <- sym
  args <- many pTypeArg
  ki <- option KHole do
    symbol' ":"
    pKind
  symbol' "="
  TypeAlias name args ki <$> pType

pTypeArg :: Parser (Name, Kind)
pTypeArg = do
    parens do
      kArg
  <|> do
    name <- var
    return (name, KHole)

pDeclType :: Parser TopLevelDecl
pDeclType = do
  symbol' "type"
  name <- sym
  args <- many pTypeArg
  ki <- option KHole do
    symbol' ":"
    pKind
  cs <- option [] do
    symbol' "="
    many pDeclCtor
  return $ Type name args ki cs

pDeclLifted :: Parser TopLevelDecl
pDeclLifted = do
  Decl <$> pDecl

pKind :: Parser Kind
pKind = OP.makeExprParser kTerm
  [ [ OP.InfixR do symbol' "->" $> KArrow ] ]
  where
    kTerm = choice
      [ symbol' "Type"       $> KStar
      , symbol' "Constraint" $> KConstraint
      , brackets pKind      <&> KList
      , var                 <&> KVar
      , symbol' "?"          $> KHole
      , parens pKind
      ]

pType :: Parser Type
pType = OP.makeExprParser tApp
  [ [ OP.InfixR do symbol' "::" $> TListCons ]
  , [ OP.InfixR do symbol' "->" $> TArrow ]
  ]
  where
    tApp = do
      f : xs <- some tTerm
      return (foldl TApp f xs)

tTerm :: Parser Type
tTerm = choice
  [ var         <&> TRigid
  , pQSym       <&> TConst
  , symbol' "?"  $> THole
  , forall_
  , symbol' "[]" $> TListEmpty
  , brackets typeList
  , brackets typeListSplice
  , parens pType
  ]
  where
    typeList = do
      list <- pType `sepBy`colon
      return (foldr TListCons TListEmpty  list)

    typeListSplice = do
      list <- pType `sepEndBy` colon
      symbol' ".."
      end <- pType
      return (foldr TListCons end list)

    forall_ = do
      symbol' "/\\"
      n <- var
      k <- option KHole do
        symbol' ":"
        pKind
      symbol' "."
      ctx <- option [] do
        chain do
          pType
      symbol' "=>"
      TForall n k ctx <$> pType

pPat :: Parser Pat
pPat = choice
  [ var <&> PVar
  , pPCtor
  , pLit <&> PLit
  , symbol' "_" $> PWild
  , pPatList
  ]
  where
    pPCtor = do
      name <- pQSym
      fields <- option [] do
        braces $ do
            n <- var
            p <- optional do
              symbol' "="
              pPat
            return (n, p)
          `sepBy`
            colon
      return $ PSym name fields

pPatList :: Parser Pat
pPatList = do
  brackets do
    (ps, lst) <- pPatListAux
    return $ PList ps lst
  where
    pPatListAux :: Parser ([Pat], Maybe Pat)
    pPatListAux = do
        symbol' ".."
        p <- pPat
        return ([], Just p)
      <|> do
        p <- pPat
        do
            colon
            (ps, mb) <- pPatListAux
            return (p : ps, mb)
          <|>
            return ([p], Nothing)
      <|> do
        return ([], Nothing)

pLit :: Parser Lit
pLit = L.lexeme space' do
  choice
    [ try L.float   <&> F
    , L.decimal     <&> I
    , stringLiteral <&> S . fromString
    ]

pDeclCtor :: Parser Ctor
pDeclCtor = do
  symbol' "|"
  name <- sym
  fields <- option [] do
    block pFieldTy
  ty <- option THole do
    symbol' ":"
    pType
  return (Ctor name fields ty)

pFieldTy :: Parser (Name, Type)
pFieldTy = do
  name <- var
  symbol' ":"
  ty <- pType
  return (name, ty)

pProg :: Parser Prog
pProg = do
  app <- pApp
  hasTy <- option id do
    symbol' ":"
    flip HasType <$> pType
  return (hasTy app)

pApp :: Parser Prog
pApp = do
  f  <- pTerm
  xs <- many pTermCall

  let
    add f' (Left  p) = App f' p
    add f' (Right t) = APP f' t

  return $ foldl add f xs

pTermCall :: Parser (Either Prog Type)
pTermCall = do
    symbol' "@"
    Right <$> tTerm
  <|>
    Left <$> pTerm

pTerm :: Parser Prog
pTerm = choice
  [ pConstruct
  , parens pProg
  , pLam
  , pLAM
  , pLet
  , pMatch
  , pList
  , pQVar <&> Var
  , pLit <&> Lit
  ]

pList :: Parser Prog
pList = do
  List <$> do
    brackets do
      do
          splices <- option False do symbol' ".." $> True
          ele <- pProg
          return (splices, ele)
        `sepBy`
          colon

pMatch :: Parser Prog
pMatch = do
  symbol' "case"
  subj <- pProg
  symbol' "of"
  alts <- many pAlt
  return (Match subj alts)

pAlt :: Parser Alt
pAlt = do
  symbol' "|"
  p <- pPat
  symbol' "=>"
  Alt p <$> pProg

pConstruct :: Parser Prog
pConstruct = do
  name <- pQSym
  fields <- option [] do
    block pField
  return $ Sym name fields

pField :: Parser (Name, Maybe Prog)
pField = do
  name <- var
  body <- optional do
    symbol' "="
    pProg
  return (name, body)

pLam :: Parser Prog
pLam = do
  symbol' "\\"
  args <- arg `sepBy1` colon
  symbol' "=>"
  Lam args <$> pProg

arg :: Parser (Name, Type)
arg = do
  name <- var
  ty <- option THole do
    symbol' ":"
    pType
  return (name, ty)

pLAM :: Parser Prog
pLAM = do
  symbol' "/\\"
  args <- kArg `sepBy1` colon
  symbol' "."
  ctx <- option [] do
    chain pType
      <* symbol' "=>"
  LAM args ctx <$> pProg

kArg :: Parser (Name, Kind)
kArg = do
  name <- var
  ki <- option KHole do
    symbol' ":"
    pKind
  return (name, ki)

pLet :: Parser Prog
pLet = do
  symbol' "let"
  ds <- block pDecl
  Let ds <$> pProg

pDecl :: Parser Decl
pDecl = choice
  [ pDeclVal
  ]

pDeclVal :: Parser Decl
pDeclVal = do
  name <- var
  ty   <- option THole do
    symbol' ":"
    pType
  symbol' "="
  Val name ty <$> pProg

stringLiteral :: Parser [Char]
stringLiteral = LC.char '"' >> manyTill L.charLiteral (LC.char '"')

var :: Parser Name
var = L.lexeme space' do
  try do
    n   <- oneOf varStart
    ame <- many $ oneOf varRest
    guard ((n:ame) `notElem` reserved) <?> "name"
    return $ fromString (n : ame)

pQVar :: Parser QName
pQVar = do
  (ps, end) <- try aux
  return (QName ps end)
  where
    aux = do
        s <- sym
        symbol' "."
        (p, end) <- aux
        return (s : p, end)
      <|> do
        v <- var
        return ([], v)

reserved :: [String]
reserved = words "case of let type Type private alias class instance as import where module Constraint"

sym :: Parser Name
sym = L.lexeme space' do
  try do
    n   <- oneOf symStart
    ame <- many $ oneOf varRest
    guard ((n:ame) `notElem` reserved) <?> "constructor"
    return $ fromString (n : ame)

pQSym :: Parser QName
pQSym = do
  (ps, end) <- try aux
  return (QName ps end)
  where
    aux = do
      s <- sym
      do
          symbol' "."
          (p, end) <- aux
          return (s : p, end)
        <|> do
          return ([], s)

block :: Parser a -> Parser [a]
block p = braces $ p `sepBy`colon

chain :: Parser a -> Parser [a]
chain p = p `sepBy`colon

colon :: Parser ()
colon = symbol' ";" <|> symbol' ","

brackets :: Parser a -> Parser a
brackets  = between (symbol' "[") (symbol' "]")

braces :: Parser a -> Parser a
braces  = between (symbol' "{") (symbol' "}")

parens :: Parser a -> Parser a
parens  = between (symbol' "(") (symbol' ")")

space' :: Parser ()
space' = L.space LC.space1 (L.skipLineComment "//") (L.skipBlockCommentNested "/*" "*/")

symbol' :: Text -> Parser ()
symbol' = void . L.symbol space'

varStart :: [Char]
varStart = ['a'.. 'z'] ++ "_"

symStart :: [Char]
symStart = ['A'.. 'Z']

varRest :: [Char]
varRest  = ['a'.. 'z'] ++ "-!?" ++ ['A'.. 'Z'] ++ ['0'.. '0']
