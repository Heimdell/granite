{-# OPTIONS_GHC -Wno-orphans   #-}

module AST.Pretty where

import AST.Types
import Pretty
import Name

instance Pretty Con Module where
  prettyPrec _ = \case
    Module na nass tlds ->
      kw "module" <+> qpath na <+> "where"
      $$ vsep (map pretty nass)
      $$ "  "
      $$ vsep (map pretty tlds)

deriving via PP Con Module instance Show Module

instance Pretty Con Import where
  prettyPrec _ = \case
    Import path (Just rename) items ->
      (kw "import" <+> qpath path
        <:> "as" <:> qpath rename)
        <:> block (map pretty items)

    Import path Nothing  items ->
      kw "import" <+> qpath path
        <:> block (map pretty items)

qpath :: [Name] -> Doc Con
qpath = foldl1 (\a b -> a <> punc "." <> b) . map (fld . pretty)

deriving via PP Con Import instance Show Import

instance Pretty Con Kind where
  prettyPrec prec = \case
    KStar -> kw "Type"
    KList ki -> op "[" <> pretty ki <> op "]"
    KArrow ki ki' -> close (punc "(") (punc ")") (prec < 7) $ prettyPrec 6 ki <+> op "->" <+> prettyPrec 7 ki'
    KVar na -> var (pretty na)
    KHole -> kw "?"
    KConstraint -> kw "Constraint"

deriving via PP Con Kind instance Show Kind

instance Pretty Con Type where
  prettyPrec prec = \case
    TVar na -> var (pretty na)
    TConst na -> constant (pretty na)
    TRigid na -> private (pretty na)
    TArrow ty ty' -> parens (prec < 7) $ prettyPrec 6 ty <+> op "->" <+> prettyPrec 7 ty'
    TApp ty ty' -> parens (prec < 5) $ prettyPrec 5 ty  <+> prettyPrec 4 ty'
    TForall na ki ctx ty -> kw "forall" <+> decl (pretty na) <+> punc ":" <+> pretty ki <> punc "." <+> chain (map pretty ctx) <+> punc "=>" <+> prettyPrec 7 ty
    TListEmpty -> kw "[]"
    TListCons ty ty' -> parens (prec < 6) $ prettyPrec 5 ty <+> op "::" <+> prettyPrec 6 ty'
    THole -> kw "?"

deriving via PP Con Type instance Show Type

instance Pretty Con Prog where
  prettyPrec prec = \case
    Var na -> var (pretty na)
    Sym na fields -> if null fields then constant (pretty na) else constant (pretty na) <+> block (map fieldVal fields)
    App pr pr' -> parens (prec < 5) $ prettyPrec 5 pr  <+> prettyPrec 4 pr'
    APP pr ty -> parens (prec < 5) $ prettyPrec 4 pr  <+> unpretty ty
    Lam x0 pr -> list (kw  "\\") (punc ",") (kw "=>") (map arg x0) <:> pretty pr
    LAM x0 ctx pr -> list (kw "/\\") (punc ",") (kw "." <+> if null ctx then mempty else chain (map pretty ctx) <+> punc "=>") (map arg x0) <:> pretty pr
    Let des pr -> kw "let" <:> block (map pretty des) $$ pretty pr
    Match pr alts -> (kw "case" <:> pretty pr) <+> kw "of" <:> vsep (map pretty alts)
    Lit lit' -> pretty  lit'
    HasType prog ty -> parens (prec < 1) $ prettyPrec 1 prog <+> punc ":" <:> prettyPrec 1 ty
    List eles -> list' (kw "[") (punc ",") (kw "]") $ map splice eles

splice :: (Bool, Prog) -> Doc Con
splice (yes, ele) = (if yes then ".." else "") <> pretty ele

qname :: [Name] -> Doc Con
qname = cat . map ((<> punc ".") . fld . pretty)

deriving via PP Con Prog instance Show Prog

instance Pretty Con QName where
  prettyPrec _ (QName path name) = qname path <> pretty name

deriving via PP Con QName instance Show QName

instance Pretty Con Decl where
  prettyPrec _ = \case
    Val na ty pr -> decl (pretty na) <:> punc ":" <+> pretty ty <:> kw "=" <+> pretty pr

instance Pretty Con a => Pretty Con (Private a) where
  prettyPrec _ = \case
    Private priv p ->
      (if priv then private "private" else mempty) <+> pretty p

deriving via PP Con Decl instance Show Decl

instance Pretty Con TopLevelDecl where
  prettyPrec _ = \case
    Decl decl' -> pretty decl'

    Class req name args sigs ->
      ((kw "class" <:> chain (map pretty req)) <:> (kw "=>" <:> pretty (foldl TApp (tcon name) (map (tcon . fst) args))))
        <:> kw "where"
          <:> block (map pretty sigs)

    Instance req name args impls ->
      ((kw "instance" <:> chain (map pretty req)) <:> (kw "=>" <:> pretty (foldl TApp (tcon name) args)))
        <:> kw "where"
          <:> block (map pretty impls)

    Type na args ki cts ->
        (kw "type" <+> decl (pretty na) <:> sep (map mkArg args)) <:> punc ":" <+> pretty ki <:> kw "=" <:> block (map pretty cts)

    TypeAlias name args ki body ->
        (kw "type alias" <+> decl (pretty name) <:> sep (map mkArg args)) <:> punc ":" <+> pretty ki <:> kw "=" <:> pretty body

mkArg :: (Pretty Con x1, Pretty Con x2) => (x1, x2) -> Doc Con
mkArg (n, k) = parens' $ pretty n <+> punc ":" <+> pretty k


deriving via PP Con TopLevelDecl  instance Show TopLevelDecl

instance Pretty Con DeclSig where
  prettyPrec _ = \case
    ValSig na ty -> decl (pretty na) <:> punc ":" <+> pretty ty
    TypeSig na ki -> decl (pretty na) <:> punc ":" <+> pretty ki

deriving via PP Con DeclSig instance Show DeclSig

instance Pretty Con Alt where
  prettyPrec _ = \case
    Alt pat pr -> kw "|" <!+!> pretty pat <+> punc "=>" <:> pretty pr

deriving via PP Con Alt instance Show Alt

instance Pretty Con Lit where
  prettyPrec _ = \case
    I n -> lit (viaShow n)
    F x -> lit (viaShow x)
    S txt -> lit (viaShow txt)

deriving via PP Con Lit instance Show Lit

instance Pretty Con Pat where
  prettyPrec _ = \case
    PVar na -> decl (pretty na)
    PSym na x0 -> constant (pretty na) <:> block (map fieldVal x0)
    PLit lit' -> pretty lit'
    PList ele Nothing -> list' (kw "[") (punc ",") (kw "]") $ map pretty ele
    PList ele (Just rest) -> list' (kw "[") (punc ",") (kw "]") $ map pretty ele ++ [punc ".." <> pretty rest]
    PWild -> kw "_"

deriving via PP Con Pat  instance Show Pat

instance Pretty Con Ctor where
  prettyPrec _ = \case
    Ctor na x0 ty ->
      (decl (pretty na) <:> block (map field x0))
        <:> punc ":" <+> pretty ty

deriving via PP Con Ctor instance Show Ctor

field :: Pretty Con x => (Name, x) -> Doc Con
field (n, p) = fld (pretty n) <+> ":" <:> pretty p

fieldVal :: Pretty Con x => (Name, Maybe x) -> Doc Con
fieldVal (n, Just p) = fld (pretty n) <+> "=" <:> pretty p
fieldVal (n, Nothing) = decl (pretty n)

fld :: Doc Con -> Doc Con
fld = dim Magenta

arg :: Pretty Con x => (Name, x) -> Doc Con
arg (n, ty) = decl (pretty n) <+> punc ":" <+> pretty ty

block :: [Doc Con] -> Doc Con
block = list (punc "{") (punc ";") (punc "}")

chain :: [Doc Con] -> Doc Con
chain = list (punc "(") (punc ",") (punc ")")

parens :: Bool -> Doc Con -> Doc Con
parens = close (punc "(") (punc ")")

parens' :: Doc Con -> Doc Con
parens' = close (punc "(") (punc ")") True

constant :: Doc Con -> Doc Con
constant = color Magenta

lit :: Doc Con -> Doc Con
lit = bright Green

punc :: Doc Con -> Doc Con
punc = bright Black

op :: Doc Con -> Doc Con
op = dim Green

var :: Doc Con -> Doc Con
var = dim Yellow

kw :: Doc Con -> Doc Con
kw = color Blue

decl :: Doc Con -> Doc Con
decl = bright Yellow

private :: Doc Con -> Doc Con
private = dim Red

test :: IO ()
test = print $ pretty $ tcon "Tuple" $: tvar "a" !: (tvar "b" $: tvar "c" --> tcon "d") !: nil --> tvar "d"
