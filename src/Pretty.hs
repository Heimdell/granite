
module Pretty where

import Data.Text qualified as Text
import Data.Text (Text)
import Control.Monad (foldM)
import Data.Function ((&))
import Data.String (IsString(fromString))
import Debug.Trace ()

newtype PP a x = PP { unPP :: x }

instance (Show (Doc a), Pretty a x) => Show (PP a x) where
  show = show . pretty . unPP

class Pretty a x | x -> a where
  prettyPrec :: Int -> x -> Doc a

pretty :: Pretty a x => x -> Doc a
pretty = prettyPrec 10

unpretty :: Pretty a x => x -> Doc a
unpretty = prettyPrec 0

close :: Doc a -> Doc a -> Bool -> Doc a -> Doc a
close start end True txt = HSep False False [start, txt, end]
close _     _   _    txt = txt

list :: Doc a -> Doc a -> Doc a -> [Doc a] -> Doc a
list start _ end []  = start <> end
list start _ end [l] = start <> l <> end
list start comma end (l : ls) =
  vsep
    $  [ start <!+!> l ]
    ++ map (comma <!+!>) ls
    ++ [ end ]

list' :: Doc a -> Doc a -> Doc a -> [Doc a] -> Doc a
list' start _ end []  = start <> end
list' start _ end [l] = start <> l <> end
list' start comma end ls = start <> aux ls <> end
  where
    aux [] = mempty
    aux [l] = l
    aux (x : xs) = x <> comma <+> aux xs

data Doc a
  = Annot a (Doc a)
  | HSep Bool Bool [Doc a]
  | VSep [Doc a]
  | Indent (Doc a)
  | Plain Text (Maybe (Int, Int))
  | Empty

data Image = Image
  { iH, iW :: Int
  , dX     :: Int
  , iBody  :: [Text]
  }
  deriving stock (Show)

data Config a = Config
  { cMaxWidth :: Int
  , cIndent   :: Int
  , cAnnotate :: a -> Image -> Image
  }

toTextLines :: Config a -> Doc a -> Image
toTextLines config = go
  where
    go = \case
      Annot a doc -> cAnnotate config a (go doc)
      HSep f ws docs -> do
        let ls = map go docs
        if f
        then hcatImagesForce ws ls
        else do
          case hcatImages ws ls of
            Just res -> res
            Nothing  -> vcatImages ls

      VSep docs -> do
        let ls = map go docs
        vcatImages ls

      Indent doc -> do
        let img = go doc
        img { dX = dX img + cIndent config }

      Plain txt mbSize ->
        maybe (Image 1 (Text.length txt) 0 [txt])
          (\(w, h) -> Image h w 0 [txt])
          mbSize

      Empty ->
        Image 0 0 0 []

hcatImagesForce :: Bool -> [Image] -> Image
hcatImagesForce ws = foldr catImg (Image 0 0 0 [])
  where
    catImg (Image h w dx txts) (Image h' w' _ txts') = do
      let h'' = max h h'
      let w'' = w + w' + if ws then 1 else 0
      Image h'' w'' dx
        $ zipWith glue (pad h'' (Text.replicate w " ") txts) (pad h'' (Text.replicate w' " ") txts')

    glue l r | ws = l <> " " <> r
    glue l r      = l <> r

pad :: Int -> a -> [a] -> [a]
pad n x ls = take n $ ls <> repeat x

pad' :: Int -> Int -> Text
pad' n len = Text.replicate (max 0 (n - len)) " "

oneLiner :: Image -> Bool
oneLiner img = iH img == 1 && iW img < 80

hcatImages :: Bool -> [Image] -> Maybe Image
hcatImages ws = foldM catImg (Image 0 0 0 [])
  where
    catImg (Image 0 0 _ _) img = Just img
    catImg img (Image 0 0 _ _) = Just img
    catImg (Image 1 w x [txt]) (Image 1 w' _ [txt']) =
      Just $ Image 1 (w + w' + if ws then 1 else 0) x [txt <> (if ws then " " else "") <> txt']
    catImg _ _ = Nothing

vcatImages :: [Image] -> Image
vcatImages = foldr catImg (Image 0 0 0 [])
  where
    catImg (Image h w dx txts) (Image h' w' dx' txts') =
      Image (h + h') (max w w') (min dx dx')
        $  map (indent (dx - dx') . (<> pad' (max w w') w)) txts
        <> map (indent (dx' - dx) . (<> pad' (max w w') w')) txts'

indent :: Int -> Text -> Text
indent n = (<>) $ Text.replicate n " "

imageToString :: Image -> String
imageToString (Image _ _ dx txts) =
  map (indent dx) txts
    & Text.intercalate "\n"
    & Text.unpack

data Color
  = Black
  | Red
  | Green
  | Yellow
  | Blue
  | Magenta
  | Cyan
  | White
  deriving stock (Enum)

data Brightness
  = Dim
  | Normal
  | Bright

type Con = (Color, Brightness)

withColors :: Config Con
withColors = Config
  { cMaxWidth = 160
  , cIndent   = 2
  , cAnnotate = \(col, br) img -> do
      let start = "\x1b[" ++ show (30 + fromEnum col) ++ unBr br ++ "m"
      let end   = "\x1b[0m"
      let
        ls' = case iBody img of
          [] -> []
          [l] -> [Text.pack start <> l <> Text.pack end]
          l : ls -> (Text.pack start <> l) : (init ls ++ [last ls <> Text.pack end])
      img { iBody = ls' }
  }
    where
      unBr Normal = ""
      unBr Bright = ";1"
      unBr Dim    = ";2"

instance Show (Doc Con) where
  show = imageToString . toTextLines withColors

color :: Color -> Doc Con -> Doc Con
color c = Annot (c, Normal)

dim :: Color -> Doc Con -> Doc Con
dim c = Annot (c, Dim)

bright :: Color -> Doc Con -> Doc Con
bright c = Annot (c, Bright)

infixr 6 <!+!>
(<!+!>) :: Doc a -> Doc a -> Doc a
HSep True True l <!+!> HSep True True r = HSep True True (l <> r)
HSep True True l <!+!> r                = HSep True True (l <> [r])
l                <!+!> HSep True True r = HSep True True ([l] <> r)
l                <!+!> r                = HSep True True [l, r]

infixr 6 <+>
(<+>) :: Doc a -> Doc a -> Doc a
HSep False True l <+> HSep False  True r = HSep False True (l <> r)
HSep False True l <+> r                  = HSep False True (l <> [r])
l                 <+> HSep False  True r = HSep False True ([l] <> r)
l                 <+> r                  = HSep False True [l, r]

infixr 4 $$
($$) :: Doc a -> Doc a -> Doc a
VSep l $$ VSep r = VSep (l <> r)
VSep l $$ r      = VSep (l <> [r])
l      $$ VSep r = VSep ([l] <> r)
l      $$ r      = VSep [l, r]

instance Semigroup (Doc a) where
  HSep False  False l <> HSep False  False r = HSep False False (l <> r)
  HSep False  False l <> r                   = HSep False False (l <> [r])
  l                   <> HSep False  False r = HSep False False ([l] <> r)
  l                   <> r                   = HSep False False [l, r]

instance Monoid (Doc a) where
  mempty = Empty

instance IsString (Doc a) where
  fromString = text . fromString

vsep :: [Doc a] -> Doc a
vsep = VSep

sep :: [Doc a] -> Doc a
sep = HSep False  True

cat :: [Doc a] -> Doc a
cat = HSep False  False

text :: Text -> Doc a
text = flip Plain Nothing

viaShow :: Show x => x -> Doc a
viaShow = fromString . show

infixl 5 <:>
(<:>) :: Doc a -> Doc a -> Doc a
l <:> r = HSep False True [l, Indent r]

-- test
--   = print $ "colors" <:> list "[" "," "]"
--     [ color Red "red"         <+> close "(" ")" False (bright Red "red" <+> dim Red "red")
--     , color Green "green"
--     , color Blue "blue"
--     , color Cyan "cyan"
--     , color Black "black"
--     , color Magenta "magenta"
--     , color Yellow "yellow"
--     , color White "white"
--     ]
