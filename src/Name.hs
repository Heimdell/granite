{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}
module Name where

import Data.Text (Text)
import Data.String (IsString (fromString))
import Pretty
import GHC.Generics (Generic)
import Data.Data (Data)
import Control.Monad.State (MonadState (get), modify)

data Name = Name
  { nRaw   :: Text
  , nIndex :: Int
  }
  deriving stock (Eq, Ord)
  deriving stock (Data, Generic)

instance IsString Name where
  fromString n = Name (fromString n) 0

instance Pretty Con Name where
  prettyPrec _ (Name raw 0)  = text raw
  prettyPrec _ (Name raw ix) = text raw <> "'" <> viaShow ix

deriving via PP Con Name instance Show Name

newtype Fresh = Fresh { getFresh :: Int }
  deriving stock (Show)

refresh :: MonadState Fresh m => Name -> m Name
refresh name = do
  modify (Fresh . (+ 1) . getFresh)
  Fresh nIndex <- get
  return name {nIndex}
