
/*
  This module is a parser test.

  This is a multiline comment.
*/

// Optional module header
//
module Test.Typecheck where

const = \a, b => a;

type Either a b =
  | Left  { left  : a } : Either a b
  | Right { right : b } : Either a b
;

id = \a => a;

list-pat = case list of
  | [] => 1
  | [a] => a
  | [a, b] => a
  | [a, b, ..c] => a
;

either = \f, g, e => case e of
  | Left  { left  = a } => f a
  | Right { right = b } => g b
;

apply = \f, x => f x;
