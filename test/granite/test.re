
/*
  This module is a parser test.

  This is a multiline comment.
*/

// Optional module header
//
module Test.Sanity.Syntax where

// imports by default are qualified
//
import Data.Eq;

// you can rename modules
//
import Data.Eq as Control.Monad.State;

// you can expose some stuff as unqualified
//
import Data.Eq { state, StateT, runState };

// you can expose some stuff as unqualified _and_ import everything else qualified
//
import Data.Eq as Control.Monad.State { state, StateT, runState };

// declare constant with type
//
literal : Int = 1;

// declare constant with inferred type
//
variable = foo;

// can call functions, `foo(foo)(bar(kek))` in JS
//
application = (foo foo) (bar kek);

// can specify templates, `foldMap<Eq<List>>` in TS
//
type-application = foldMap @(Eq List);

// this is the only way to declare functions
//
lambda = \a => a;

// and you can do local ones
//
lambda = \a =>
  let
    { const = \x, y => x
    }
  const a a
;

// we're not limited to single arg
//
lambda-many-args = \a, b, c => a;

// you can specify types
//
lambda-many-args-with-types = \a : Int, b : List a, c => a;

// you can specify _result_ type
//
lambda-many-args-with-result-type =
  \ a : Int
  , b : List a
  , c
  => a : d;

// this is how you write generics
//
generic-lambda = /\a : Type . \a : a => a : a;
generic-lambda = /\a        . \a : a => a : a;

// generics can acceps generics as arguments
//
generic-f = /\f : Type -> Type . Functor f => \box : f a => box : f B;
generic-f = /\f                .              \box : f a => box : f B;

// and also multiple types
//
generic-many-params =
  /\ a, b .
  \  f : a -> b
  ,  l : List a
  =>
    l : List b;

// we have integers, floats and strings
//
local-defs =
  let
    { a = 1
    , b = plus a 1.345
    ; d = "foo\nbar\"quz"
    }
  let
    { c = minus b 4
    }
  c;

// We also have algebraic datatypes.
//
// Here is an example literal of a Pair "struct".
//
constructors =
  Pair { fst = 1, snd = local-defs };

// And this is how you can take it apart (for now).
//
case-split =
  case constructors of
  | Pair {fst, snd = s} => plus fst s
  | UnPair {trd = "fdf"} => "nope"
;

// we have lists, they can splice
//
list-pat = case list of
  | [] => 1
  | [a] => a
  | [a, b] => a
  | [a, b, ..c] => a
;

// we have lists, they can splice _everywhere_ in expression form
//
list-expr =
  [ []
  , [1]
  , [a, b]
  , [..a, b]
  , [..a, b, ..c]
  , [..a, ..b, ..c]
  ]
;

// you can call stuff qualified!
//
qualified-names =
  [ Data.Eq.equality
  ; Control.Monad.State.StateT
  ]
;

// All types are enums with possible params.
//
// This one has 1 constructors and no fields.
//
type Unit =
  | Unit;

// This one has no constructors and cannot be created.
//
type Void;

// This is a list, it has type argument and 2 constructors.
//
type List : Type -> Type =
  | Cons { head : a, tail : List a} : List a
  | Empty : List a
;

// You can sometimes drop annotations (I believe I can infer them).
//
type List a =
  | Cons { head : a, tail : List a} : List a
  | Empty
;

// Type parameters can be named and kind-annontated.
//
// Kind is a type of types.
//
// Usable kinds are, where `k` is any kind
//   - Type
//   - Constraint
//   - [k]       -- list of types, special case and syntax
//   - k1 -> k2  -- kind of a generic type
//
type Tuple (elements : [Type]) : Type =
  | Cons { head : x, tail : Tuple xs} : Tuple (x :: xs)
  | Empty : Tuple []
;

// Type aliases!
//
type alias Matrix (a : Type) = List (List a)
;

// RankNTypes! (I _hope_ I can deal with them correctly)
//
type alias Lens s t a b = /\f. Functor f => (a -> f b) -> (s -> f t);

// Type classes!
//
class Functor f where {
  map : (a -> b) -> f a -> f b
};

class (Functor f) => Applicative f where {
  pure : a -> f a;
  ap : f (a -> b) -> f a -> f b
};

// Any declaration except instance one can be private.
//
private class (Applicative f) => Monad f where {
  bind : (a -> f b) -> f a -> f b
};

// MTPC! No fundeps yes.
//
class (Monoid a, Eq a) => Action a s where {
  act : a -> s -> s
};

// I doubt I can implement that, but uh okay.
//
type Dict : Constraint -> Type =
  | Dict : /\c : Constraint. c => Dict c
;

id = \a => a;
